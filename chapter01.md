# Initial components

[https://tribecoders.com/petwars/chapter01](https://tribecoders.com/petwars/chapter01)

Main concept of **React** is to build applications from simple components. Our components can have properties and their state. More complex components can use other smaller components in a tree like structure. At the beginning we will define main components and connect them to each other.

## Game Structure
To start defining components we need some information about the game we are creating. Lets start from game brief. In Pet Wars each player will be dealt a hand full of cards and will be placing them on the board (battlefield) against cards played by opponent. Cards position will shift on the board as new cards will be placed. Similar with the player cards (hand). It will be replenished after each card use. Pet Wars will be a server based game with main game logic placed on the server. Client will be used mainly to present current game state and user interaction.

## Components
We have our game initial brief. From that we can create main application structure. This structure may shift in the future but by using **React** we can do that easily. 

### Game
This will be our main component displaying game content. It will be the trunk of our components tree. **Game** will present **Battlefield** and a player **Hand**. 

### Battlefield
This will be a game board that we will divide into 3x6 grid. That will be spaces for cards played by players.

### Hand
A simple array of cards that player holds.

### Card
Both **Battlefield** and **Hand** will be displaying cards. This will be for our final component. 

## Demo
You can see our application structure below 
`https://tribecoders.com/petwars/chapter01`