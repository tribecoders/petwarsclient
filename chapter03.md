# Scaling

As you can see in previous chapter our application do not display correctly on smaller screens. This is problematic. We could adjust it size using **"responsive design"**. We already using this for some layout elements. 

With graphics and games it is good to keep the proportions and constant element sizes. In this case it would be great, if we keep all card graphics proportion and game board at constant pixel size. In this way designers will have easier life preparing game graphics once.

We can achieve this using html tag

```tansform: scale()```

## Keep all element at the same scale
Lets introduce central scale manager which will count the scale all of the elements. Our base screen size will be 1280x760 px. Our designers should prepare graphics for that resolution screen. On other screens all of the graphics will be scaled to fit the window.

## Origin
To keep the layout we can adjust transform origin. To make element shrink/expand keeping his centrer as the middle point. We need to set

```transform-origin: center```

You can experiment with other transform origins: top, bottom or even top left

## Applying the scale
We created a styled component that will add scaling effect to any other component. We keep transform origin to the center. We can switch this to a style component property in the future.


## Subscribing for a resize
If our user change the size of the screen or rotate a phone, we will persist current scale and may overlap the screen. To avoid this we may subscribe to a resize event and update the scale each time size changes. We need to remember to unsubscribe when React component is destroyed. 

```
componentDidMount() {
  this.scaleManager.subscribe(this.updateScale.bind(this));
}

componentWillUnmount() {
  this.scaleManager.unsubscribe();
}
```
