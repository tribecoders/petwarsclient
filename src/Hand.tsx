import React from "react";
import styled from 'styled-components';
import { Card } from "./Card";
import { observer, inject } from "mobx-react";
import Pet from "./Pet";
import { HandStore } from "./HandStore";
import Selector from "./Selector";

export interface HandProps {
  handStore?: HandStore;
}

export interface HandState {
  selectedPet: number | undefined;
}

@inject('handStore')
@observer
export default class Hand extends React.Component<HandProps, HandState> {
  state = {
    selectedPet: undefined
  }

  selectPet(petNumber: number) {
    if (this.state && this.state.selectedPet === petNumber) {
      this.setState({
        selectedPet: undefined
      });
    } else {
      this.setState({
        selectedPet: petNumber
      });
    }
  }

  placePet(position: number) {
    if (this.props.handStore && typeof this.state.selectedPet !== 'undefined') { //selected pet can be 0
      this.props.handStore.playPet(this.state.selectedPet!, position);
    }
  }

  render() {
    const cards = [];
    if (this.props.handStore) {
      for (let i = 0; i < 4 ; i++) {
        if (this.props.handStore.hand[i]) {
          const pet = this.props.handStore.hand[i]!;
          cards.push(
            <CardHolder key={i}>
              <Selector disable={this.props.handStore.isDisabled} select={() => {this.selectPet(i)}} placePet={this.placePet.bind(this)} selected={this.state && this.state.selectedPet === i}>
                <Pet pet={pet}></Pet>
              </Selector>
            </CardHolder>)
        } else {
          cards.push(<CardHolder key={i}><Card imageUrl={'empty'}></Card></CardHolder>)
        }
      }
    }

    return (
      <HandGrid>
        {cards}
      </HandGrid>
    )
  }
}

const CardHolder = styled.div`
  margin: 0 5px;
  cursor: pointer;
`;

const HandGrid = styled.div`
  width: 860px;
  height: 140px;
  background-color: lightgray;
  display: flex;
  justify-content: space-between;

  position: relative;
`;