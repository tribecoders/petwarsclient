import { observable, action } from "mobx";
import { IPet } from "./interfaces";

export class BattlefieldStore {
  rowCount = 3;
  lineCount = 6;
  @observable battlefield: (IPet | undefined)[][];

  constructor() {
    this.battlefield = Array.from(Array(this.rowCount), () => Array(this.lineCount).fill(undefined));
  }

  @action
  clean() {
    this.battlefield = [];
    for (let i = 0; i < this.rowCount; i++) {
      this.battlefield.push(new Array(this.lineCount));
    }
  }

  @action
  setPets(pets: (IPet | undefined)[][], enemy = false) {
    pets.forEach((row, rowIndex) => {
      row.forEach((pet, lineIndex) => {
          if (pet) {
            pet.friendly = !enemy;
            if (enemy) {
              this.battlefield[rowIndex][lineIndex] = pet;
            } else {
              this.battlefield[rowIndex][this.lineCount - lineIndex - 1] = pet;
            }
        }
      });
    });
  }
}

export const battlefieldStore = new BattlefieldStore();