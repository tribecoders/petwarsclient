export default class ScaleManager {
  public scale: number = 1;
  private update?: (scale: number) => void;
  private designWidth = 1280;
  private designHeight= 720;

  constructor() {
    this.resize();
  }

  subscribe(update: (scale: number) => void) {
    this.update = update;
    window.addEventListener('resize', this.resize.bind(this));
    this.resize();
  }

  unsubscribe() {
    window.removeEventListener('resize', this.resize);
  }

  public resize() {
    let vpw = window.innerWidth;
    let vph = window.innerHeight;
    // switch axis for portrait view
    if (window.innerWidth < window.innerHeight && window.innerWidth < 992) {
      vph = window.innerWidth;
      vpw = window.innerHeight - 100;
    }
    let nvw;
    let nvh;
    if (vph / vpw < this.designHeight / this.designWidth) {
      nvh = vph;
      nvw = (nvh * this.designWidth) / this.designHeight;
    } else {
      nvw = vpw;
      nvh = (nvw * this.designHeight) / this.designWidth;
    }
    const scaleX = nvw / this.designWidth;
    const scaleY = nvh / this.designHeight;

    this.scale = Math.min(scaleX, scaleY);
    if (this.update) {
      this.update(this.scale);
    }
  }
}
