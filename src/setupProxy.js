const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(createProxyMiddleware("/ws", {target: "http://192.168.0.80:4000", ws: true}))
  app.use(createProxyMiddleware("/api", {target: "http://192.168.0.80:4000"}))
};