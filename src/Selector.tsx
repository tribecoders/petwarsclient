import React from "react";
import styled from "styled-components";

export interface SelectorProps {
  disable: boolean
  selected: boolean
  select: () => void;
  placePet: (position: number) => void
  children?: any;
}

export default class Selector extends React.Component<SelectorProps, {}> {


  render() {
    return (
    <div onClick={this.props.select}>
      {this.props.selected && !this.props.disable &&
        <Container>
          <Position onClick={() => {this.props.placePet(0)}}>1</Position>
          <Position onClick={() => {this.props.placePet(1)}}>2</Position>
          <Position onClick={() => {this.props.placePet(2)}}>3</Position>
        </Container>
      }
      {this.props.children}
    </div>)
  }
}

const Container = styled.div`
  position: absolute;
  display: flex;
  justify-content: space-around;
  width: 200px;
  top: -50px;
`

const Position = styled.div`
  background-color: #cccccc;
  font-size: 30px;
  line-height: 40px
  width: 40px;
  text-align: center;
  border-radius: 40px;
  cursor: pointer;
`