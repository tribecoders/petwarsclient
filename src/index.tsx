import React from 'react';
import ReactDOM from 'react-dom';
import 'reset-css';
import Game from './Game';
import { handStore } from "./HandStore";
import { Provider } from 'mobx-react';
import GameClient from './GameClient';
import { battlefieldStore } from './BattlefieldStore';

const gameClient = new GameClient(battlefieldStore, handStore);

ReactDOM.render(
  <Provider battlefieldStore={battlefieldStore} handStore={handStore}>
    <Game gameClient={gameClient}></Game>
  </Provider>, document.getElementById('root')
);
