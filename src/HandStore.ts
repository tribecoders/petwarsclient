import { observable, action } from "mobx";
import { IPet } from "./interfaces";

export class HandStore {
  handCount = 4;
  @observable hand: (IPet | undefined)[];
  @observable isDisabled: boolean =  false;
  private playListners: ((cardIndex: number, position: number) => void)[] = [];

  constructor() {
    this.hand = new Array(this.handCount);
  }

  addPlayListner(play:(cardIndex: number, position: number) => void) {
    this.playListners.push(play);
  }
   
  @action
  dealPets(pets: IPet[]) {
    for (let cardIndex = 0; cardIndex < this.handCount; cardIndex++) {
      this.hand[cardIndex] = pets[cardIndex];
    }
    this.isDisabled = false;
  }

  @action
  playPet(cardIndex: number, position: number) {
    this.hand.splice(cardIndex, 1);
    this.hand.push(undefined);
    this.playListners.forEach(play => play(cardIndex, position));
    this.isDisabled = true;
  } 

}

export const handStore = new HandStore();