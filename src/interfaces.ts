export interface IPet {
  power: number;
  defence: number;
  life: number;
  image: string;
  friendly: boolean;
}

export interface IPosition {
  row: number;
  line: number;
}