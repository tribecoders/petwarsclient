import React from 'react';
import styled from 'styled-components';
import Pet from './Pet';
import { Card } from './Card';
import { observer, inject } from "mobx-react"
import { BattlefieldStore } from './BattlefieldStore';

export interface BattlefieldProps {
  battlefieldStore?: BattlefieldStore
}

@inject('battlefieldStore')
@observer
export default class Battlefield extends React.Component<BattlefieldProps, {}> {
  battleRows = 3;
  battleLines = 6;

  createGrid() {
  const rows = [];
    for (let i = 0; i < this.battleRows; i++){
      const lines = []
      for (let j = 0; j < this.battleLines; j++){
        if (this.props.battlefieldStore && this.props.battlefieldStore.battlefield[i] && this.props.battlefieldStore.battlefield[i][j]) {
          const pet = this.props.battlefieldStore.battlefield[i][j];
          lines.push(<CardCell key={'l' + j}><Pet pet={pet}></Pet></CardCell>);
        } else {
          lines.push(<CardCell key={'l' + j}><Card imageUrl={'empty'}></Card></CardCell>)
        }
      }
      rows.push(<tr key={'r' + i}>{lines}</tr>);
    }
    return rows;
  }

  render() {
    return (
      <BattlefieldContainer>
        <table>
          <tbody>
            {this.createGrid()}
          </tbody>
        </table>
      </BattlefieldContainer>
    )
  }  
}

const BattlefieldContainer = styled.div`
  width: 1270px;
  height: 450px;
`;

const CardCell = styled.td`
  border: 10px solid transparent;
`;