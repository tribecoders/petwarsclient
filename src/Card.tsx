import styled from 'styled-components';
import props from 'styled-components-ts';

export interface CardProps {
  imageUrl: string;
  shadow?: string
}

export const Card = props<CardProps>(styled.div)`
  background-image: url('images/${props => props.imageUrl}.png');
  background-size: cover;
  width: 200px;
  height: 140px;
  border-radius: 15px;
  box-shadow: 0px 0px 10px 0px ${props => props.shadow ? props.shadow : 'transparent'};
`;