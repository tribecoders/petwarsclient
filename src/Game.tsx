import React from "react";
import styled from 'styled-components';
import GameClient from "./GameClient";
import PlayScreen from "./PlayScreen";
import JoinScreen from "./JoinScreen";
import AwaitingScreen from "./AwaitingScreen";

enum GameScreen {
  Join,
  Await,
  Play,
}

export interface GameProps {
  gameClient: GameClient;
}

export interface GameState {
  screen: GameScreen,
  room: string | undefined;
}

export default class Game extends React.Component<GameProps, GameState> {
  state = {
    screen: GameScreen.Join,
    room: undefined,
  };
  onGameMessage: ((message: string) => void) | undefined

  onMessage(message: string) {
    switch (message) {
      case 'start':
        this.setState({
          screen: GameScreen.Play
        });
        break;
      case 'stop':
        this.setState({
          screen: GameScreen.Await
        })
        break;
      case 'full':
        this.setState({
          screen: GameScreen.Join
        })
        break;
      default:
        if (this.onGameMessage) {
          this.onGameMessage(message);
        }
    }
  }

  registerGame(onGameMessage: (message: string) => void) {
    this.onGameMessage = onGameMessage;
  }

  selectRoom(room: string) {
    this.props.gameClient.join(room, this.onMessage.bind(this));
    this.setState({
      room,
      screen: GameScreen.Await
    })
  }


  render() {
    return (
      <Rotate>
        <MainScreen>
          {this.state.screen === GameScreen.Join && <JoinScreen selectRoom={this.selectRoom.bind(this)} />}
          {this.state.screen === GameScreen.Await && <AwaitingScreen room={this.state.room} />}
          {this.state.screen === GameScreen.Play && <PlayScreen registerGame={this.registerGame.bind(this)} initNewGame={this.props.gameClient.initNewGame.bind(this.props.gameClient)}/>}
        </MainScreen>
      </Rotate>
    )
  }
}

const Rotate = styled.div`
  height: 100vh;
  @media screen and (max-width: 992px) and (orientation: portrait) {
    transform: rotate(-90deg);
    transform-origin: left top;
    height: 100vw;
    width: calc(100vh - 100px);
    overflow-x: hidden;
    position: relative;
    left: 0;
    top: 100%;
  }
`;
  
const MainScreen = styled.div`
  background-color: lightgray;
  height: 100%;
  width: 100%;
  text-align: center;
`