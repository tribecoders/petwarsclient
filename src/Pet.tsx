import React from "react";
import { Card } from "./Card";
import { IPet } from "./interfaces";
import styled from "styled-components";

export interface PetProps {
  pet: IPet | undefined;
}

export default class Pet extends React.Component<PetProps, {}> {
  render() {
    return (
      <div>
        {this.props.pet && 
          <Container>
              <Stats>
                <Power>{this.props.pet.power}</Power>
                <Life>{this.props.pet.life}</Life>
                <Defence>{this.props.pet.defence}</Defence>
              </Stats>
            <Card 
            imageUrl={this.props.pet.friendly ? `${this.props.pet.image}_friendly` : `${this.props.pet.image}_enemy`} 
            shadow={this.props.pet.friendly? 'blue' : 'red'}>
            </Card>
          </Container>
        }
      </div>
    )
  }
}

const Container = styled.div`
      position: relative;
    `
const Stats = styled.div`
  position: absolute;
  display: flex;
  top: 5px;
  font-size: 30px;
  width: 90%;
  justify-content: space-between;
  padding: 0 5%;
`
const Power = styled.div`
  color: red;
`
const Life = styled.div`
color: yellow;
` 

const Defence = styled.div`
  color: green;
    `