import React from "react";
import { Container, Grid, TextField, Button } from "@material-ui/core";
import styled from "styled-components";

export interface JoinScreenProps {
  selectRoom: (room: string) => void
}

export interface JoinScreenState {
  roomId: string
}

export default class JoinScreen extends React.Component<JoinScreenProps, JoinScreenState> {
  state = {
    roomId: ""
  }

  handleChange(event: any) {
    this.setState({roomId: event.target.value});
  }

  handleSubmit() {
    this.props.selectRoom(this.state.roomId);
  }

  render() {
    return (
      <JoinContainer> 
        <Container>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Title>Pet Wars</Title>
              <TextField autoFocus label="room id" variant="outlined" value={this.state.roomId} onChange={this.handleChange.bind(this)}/>
              <br/>
              <Button size="large" variant="outlined" color="primary" style={{margin: '20px'}} onClick={this.handleSubmit.bind(this)}>
                Join or Create
              </Button>
            </Grid>
          </Grid>
        </Container>
      </JoinContainer>
    );
  }
}

const JoinContainer = styled.div `
padding-top: 20px;
`;

const Title = styled.div`
font-size: 50px;
padding: 20px;
`;