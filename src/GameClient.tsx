import { HandStore } from "./HandStore";
import { BattlefieldStore } from "./BattlefieldStore";
import socketio from "socket.io-client";
import { IPet } from "./interfaces";

export default  class GameClient {
  private battlefieldStore: BattlefieldStore;
  private handStore: HandStore;
  private socket: SocketIOClient.Socket | undefined;

  constructor(battlefieldStore: BattlefieldStore, handStore: HandStore){
    this.battlefieldStore = battlefieldStore;
    this.handStore = handStore;
    
  }

  join(room: string, onMessage: (message: string) => void) {
    this.socket = socketio.connect(":4000/ws");
    this.socket.emit('create', room);
    this.handStore.addPlayListner(this.playCard.bind(this))
    this.socket.on('message', (message: string) => {
      console.log(message);
      if (message) {
        onMessage(message);
      };
    });
    this.socket.on('hand', (pets: IPet[]) => {
      pets.forEach(card => card.friendly = true);
      this.handStore.dealPets(pets);
    });
    this.socket.on('board', (pets: IPet[][]) => {
      this.battlefieldStore.clean();
      this.battlefieldStore.setPets(pets);
    });
    this.socket.on('enemy', (pets: IPet[][]) => {
      this.battlefieldStore.setPets(pets, true);
    });
  }

  playCard(cardIndex: number, position: number) {
    if (this.socket) {
      this.socket.emit('play', {cardIndex, position});
    } else {
      console.error('No connection to the server');
    }
  }

  initNewGame() {
    if (this.socket) {
      console.log('init new game');
      this.socket.emit('new'); 
    } else {
      console.error('No connection to the server');
    }
  }
}