import React from "react";
import { Container } from "@material-ui/core";
import styled from "styled-components";

export interface AwaitingScreenProps {
  room: string | undefined;
}

export default class AwaitingScreen extends React.Component<AwaitingScreenProps, {}> {
  render() {
    return (
      <AwaitingContainer>
        <Container>
          <Title>Awaiting oponent...</Title>
          Your are connected to room id: {this.props.room}
        </Container>
      </AwaitingContainer>
    );
  }
}

const AwaitingContainer = styled.div`
  padding-top: 20px;
  font-size: 20px;
`;

const Title = styled.div`
  font-size: 50px;
  padding: 20px;
`;