import React from "react";
import { Container, Button } from "@material-ui/core";
import styled from "styled-components";

export interface ResultScreenProps {
  winner: boolean;
  newGame: () => void;
}

export default class ResultScreen extends React.Component<ResultScreenProps, {}> {
  render() {
    return (
      <ResultContainer>
        <Container>
          <Title>{this.props.winner ? <span>Winner</span> : <span>Loser</span>}</Title>
          <Button size="large" variant="outlined" onClick={this.props.newGame}>New Game</Button>
        </Container>
      </ResultContainer>
    );
  }
}

const ResultContainer = styled.div`
  padding-top: 20px;
  font-size: 20px;
`;

const Title = styled.div`
  font-size: 50px;
  padding: 20px;
`;