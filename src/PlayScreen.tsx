import React from "react";
import Pet from "./Pet";
import ScaleManager from "./ScaleManager";
import styled from "styled-components";
import props from 'styled-components-ts';
import Battlefield from "./Battlefield";
import Hand from "./Hand";
import ResultScreen from "./ResultScreen";

 export interface PlayScreenProps {
  registerGame: (onGameMessage: (message: string) => void) => void
  initNewGame: () => void
}

export interface PlayScreenState {
  scale: number;
  hand: Pet[];
  countdown: number | undefined;
  winner: boolean,
  loser: boolean, 
}

export default class PlayScreen extends React.Component<PlayScreenProps, PlayScreenState> {
  scaleManager: ScaleManager = new ScaleManager();
  state = {
    scale: this.scaleManager.scale,
    hand: [],
    countdown: undefined,
    winner: false,
    loser: false,
  }
  
  componentDidMount() {
    this.props.registerGame(this.onMessage.bind(this));
    this.scaleManager.subscribe(this.updateScale.bind(this))
  }
  
  componentWillUnmount() {
    this.scaleManager.unsubscribe();
  }

  updateScale(scale: number) {
    this.setState({
      scale
    })
  }

  onMessage(message: string) {
    switch (message) {
      case 'countdown':
        this.countdown();
        break;
      case 'winner':
        this.setState({
          winner: true
        });
        break;
      case 'loser':
        this.setState({
          loser: true
        });
        break;
      case 'new':
        this.newGame();
        break;
    }
  }

  countdown() {
    let time: number = 3;
    const roundTimer = setInterval(() => {
      this.setState({countdown: time});
      time -= 1;
      if (time < 0) {
        clearInterval(roundTimer);
        this.setState({countdown: undefined});
      }
    }, 1000);
  }

  newGame() {
    this.setState({
      winner: false,
      loser: false,
    });
  }

  render() {
    
    return (
      <GameContainer height={100} color={'red'}>
        {this.state.countdown &&
          <Countdown>{this.state.countdown}</Countdown>
        }
        {this.state.winner || this.state.loser ?
          <GameContainer height={100} color={this.state.winner ? 'gold' : 'lightgray'}>
            <ResultScreen winner={this.state.winner} newGame={this.props.initNewGame} />
          </GameContainer>
          :
          <GameContainer height={100} color={'gold'}>
            <GameContainer  height={80} color={'lightgray'} flex>
            <Scaled scale={this.state.scale}>
              <Battlefield></Battlefield>
            </Scaled>
            </GameContainer>
            <GameContainer height={20} color={'gray'} flex>
            <Scaled scale={this.state.scale}>
              <Hand key="hand"></Hand>
            </Scaled>
            </GameContainer>
          </GameContainer>
        }
      </GameContainer>
    )
  }
}
export interface GameContainerProps {
  height: number;
  color: string;
  flex?: boolean;
}

const GameContainer = props<GameContainerProps>(styled.div)`
  width: 100%;
  height: ${props => props.height}%;
  background-color: ${props => props.color};
  ${props => props.flex ? 'display: flex' : ''}
  justify-content: center;
  align-items: center;
`;

export interface ScaledProps {
  scale: number;
}

const Scaled = props<ScaledProps>(styled.div)`
  transform: scale(${props => props.scale});
  transform-origin: center;
`;

const Countdown = styled.div`
  position: absolute;
  font-size: 140px;
  color: #444444;
  width: 100%;
  text-align: center;
  top: 30%;
  z-index: 1;
`;