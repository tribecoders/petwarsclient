# Server connection

At the beginning we created as many content independently. We came to a point when Pet Wars server will come in place. See our other tutorial "Learn Node.js with Pet Wars". We will establish REST and WebScokets communication. 

## CORS
Our UI **React** application is run for the development purposes by a WebPack web sever. For the final solution (production) we will also use some kind of web server. In both cases we will run everything on selected computer port (3000 development, 80 production). At the same time our Pet Wars server will also reside on a selected port (in this case 4000 for both environments). 

This will lead to calls from *localhost:3000* (UI) to *localhost:4000*(server). For a browser this are separate domains and a "Cross-Origin Resource Sharing" (CORS) error will be raised. We need to avoid this.

One of the solutions is a proxy. In this case we will tell our UI web server that all calls to *localhost:3000/api* and *localhost:3000/ws* need to be forwarded to *localhost:4000*. This way UI will always make calls to *localhost:3000* and communication will reach *localhost:4000* when needed.

How can we do that. Fortunately most of web servers comes with proxy option we just need to configure it. In this case we just need to create **setupProxy.js** file in our */src* directory and put all of the configuration in it.

```
const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(createProxyMiddleware("/ws", {target: "http://localhost:4000", ws: true}))
  app.use(createProxyMiddleware("/api", {target: "http://localhost:4000"}))
};
```

This way we tell our development server to forward selected endpoints. Remember that proxy only works from application code, you cannot test it in the browser.

## REST
REST is a transfer protocol that utilises current HTTP methods. For Java Script it is almost native with JSON as data format. We can use methods like

* GET - to get data from server
* POST - to add store data at server
* PUT - update data at server
* DELETE - remove data from server

For full list please see [https://www.w3schools.com/tags/ref_httpmethods.asp](https://www.w3schools.com/tags/ref_httpmethods.asp)

## Fetch REST
We can now test if our rest communication works. In our **GameClient** file we will call our test endpoint (see Learn Node.js with Pet Wars - chapter 2: API). We will use built in **React** fetch method.

```
const res = await fetch('api/battlefield')
const data = await res.json();
console.log(data);
```

We will log any data that we will receive.

## Socket io
For our Web Sockets implementation we will use Socket.io library It is one of the mostly known and used for that kind of communication. For more detail [https://socket.io/](https://socket.io/).

## Web Sockets
We need to import socket.io client library.

```
import socketio from "socket.io-client";
```

We just need to connect now.

```
const socket = socketio.connect("http://localhost:4000/ws");
```

And finally we will send test message
```
socket.emit("message","test");
```

Server logs should now show that server received "test" message.

