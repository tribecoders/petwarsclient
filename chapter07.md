# Taking an action
Lets start to move our static pieces a little bit. At the beginning we can place some pets on our battlefield. 

## Selecting 
We can start by adding a component that will be displayed when a pet is selected and we can chose to which lane it should go. We create "Selector" component that will wrap around or Pet.

```
<Selector select={() => {this.selectPet(i)}} placePet={this.placePet.bind(this)} selected={this.state && this.state.selectedPet === i}>
  <Pet pet={pet}></Pet>
</Selector>
```

First of all it will display our pet component accessing it through auto set property *children*. 

```
<div>
  {this.props.children}
</div>
```

We will keep track of selected Pet in our Hand so we can coordinate whole hand. We pass to Selector whether pet is selected and a select function to react to user actions. Yes you can pass functions as any other properties to a component. So when our Pet is selected we will display buttons to select a row and when user click one of them we mark it selected and deselect previous one (the same with multiple clicks)

```
<div onClick={this.props.select}>
  {this.props.selected && 
    <Container>
      ...
    </Container>
  }
</div>
```

Last thing is to place pet on the battlefield when it row is selected. This can be done with same callback approach as before. This time by returning selected value.

```
<Position onClick={() => {this.props.placePet(0)}}>1</Position>
```

## Callbacks implementation
Now we just need to implement and pass select and placePet callbacks in our hand. We just need to define proper functions in our component.

```
selectPet(petNumber: number) {
  ...
}

placePet(position: number) {
  ...
}
```

Now lets pass them to component. With select we will wrap our call in anonymous arrow function that will preserve which pet is selected in our hand. I case of place Pet we need to pass only the function. As this will be asynchronous call we need to tell JS to assign (bind) proper **this** reference, which is sometimes tricky. 

```
<Selector select={() => {this.selectPet(i)}} placePet={this.placePet.bind(this)} selected={this.state && this.state.selectedPet === i}>
```

Our actions will set proper states and do store actions updating all of our components.
