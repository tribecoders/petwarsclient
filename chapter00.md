# Pet Wars - Client

Do you want to learn **React** in a fun way. Lets build a simple game using React components. The basic concept is to have two players facing each other with a card duel (Pet Wars). 

Our game will be a player vs player game. That is why we will be developing a Pet Wars client and server. We will post a separate blog on [tribecoding.com](tribecoding.com) about Node.js server and web socket communication. 

You can find full sources of Pet Wars at [https://gitlab.com/tribecoders/petwars](https://gitlab.com/tribecoders/petwars). 

You can watch all of the steps in action at our Pet Wars demo page at [https://tribecoders.com/petwars](https://tribecoders.com/petwars)

In our project will be using:

## React
A JavaScript library that simplify process of designing an interactive UI. React will efficiently update and render our UI by updating just the right components when our data changes. See more at [https://reactjs.org/](https://reactjs.org/)

## NPM
If you are not familiar with this tool. This is a package manager for JavaScript projects. It will helps us to organise code dependencies in our ever growing code base. See [https://www.npmjs.com/](https://www.npmjs.com/) for more details.

## WebPack
This is a module bundler for modern JavaScript applications. From managing scripts and images, to compiling SCSS, TypeScript. See documentation at [https://webpack.js.org/](https://webpack.js.org/)

## TypeScript
This is optional typing library for JavaScript. Details about TypeScript can be found at [https://www.typescriptlang.org/](https://www.typescriptlang.org/). It is not mandatory for any of JavaScript projects. I strongly encourage you to use it in medium to big projects. Especially if you expect that there will be more collaborators. It gives out of the box JavaScript code a structure and keeps it easy to ready. It maybe looks a little formal at first. But after a while you will see benefits of using it. Especially for games where from a really small project you will gradually build bigger and bigger code base.

