# Applying styles

[https://tribecoders.com/petwars/chapter02](https://tribecoders.com/petwars/chapter02)

We started with empty components with just a text and structure. Now lets add a bit of colour to it. We are still web application and we can use CSS for that. **React** allows various ways to add CSS styles to a component.

## Inline CSS
We can use **Inline CSS** approach and define styles directly inside of our components. **React** give you an option to add CSS styles inline with the code. Written as variables or attributes and passed to elements. This will keep all of our component information in one place. This will also enable more complex styling with JavaScript support. So lets define our first inline CSS.

```
render() {
    const containerStyle: CSS.Properties = {
      width: '100%',
      height: '100%'
    }
    ...

    return (
      <div style={containerStyle}>
        ...
      </div>
    )
  }
```

To enable **TypeScript** types support we can use **csstype**. 

```
import CSS from 'csstype';
```

Inline CSS approach was applied to our **Game** and **Battlefield** components.

## Styled components
Styled components approach base on [styled-components](styled-components.com) library. 
We will be defining new components from html elements and adding them CSS style. We can even define properties (props) for them and change their behaviour based on data. This will leverage component syntax of the **React** library.

CSS-in-JS attaches a *style* tag on top of the DOM while inline styles just attaches the properties to the DOM node

```
render() {
  return (
    <HandGrid>
      <Card></Card>
      <Card></Card>
      <Card></Card>
      <Card></Card>
    </HandGrid>
  )
}

const HandGrid = styled.div`
  width: 860px;
  height: 140px;
  ...
`;

```

Styled components approach was applied to **Hand** and **Card** components.

## Other approaches
There is no main approach to CSS styling in **React** components. If you search the web you can find many others, like: CSS Stylesheets, CSS Modules, Stylable. You need to pick the ones that fit your needs. 

## Demo
Our game after applying styles 
`https://tribecoders.com/petwars/chapter02`