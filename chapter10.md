# Server data
In previous chapter we connected our ReactJS client to NodeJS server. There is no way to guarantee that user will not temper with client code That is why, our goal is to have "thin client". The main responsibility of a client will be to visualise game state. All of the game logic will be provided by NodeJS server. This way players will not have an option to manipulate game outcome.

## Game start 
When player connects we display empty board and hand then we need to receive initial hand cards. We just need to switch from static hand cards to a dynamic one. We will listen for a hand request and display received cards.

```
socket.on('hand', (pets: IPet[]) => {
  pets.forEach(card => card.friendly = true);
  this.handStore.dealPets(pets);
});
```

This way we will receive all future updates of cards that player holds in a hand.

## Game play
Each time a card and position will be selected. Game client will send down selected card index and the position to play card at. 

```
GameClient
playCard(cardIndex: number, position: number) {
  if (this.socket) {
    this.socket.emit('play', {cardIndex, position});
  } else {
    console.error('No connection to the server');
  }

}
```

The other player will have last 3 seconds to select the card and the round will be closed.

## Round result
Depending on pet position server will:

1. fight 2 pets that are adjacent
2. move pet forward to free field
3. place selected hand cards on the board

The final board state will be propagated and updated on the client side (similar to hand)

```
GameClient
this.socket.on('board', (pets: IPet[][]) => {
  this.battlefieldStore.clean();
  this.battlefieldStore.setPets(pets);
});
```

## Game result
When one of the pets reach last board field the player is the winner. Result screen will be presented to all players with win/lose text. Next players can go back to have another round (resetting board and hand state).


```
ResultScreen 

export interface ResultScreenProps {
  winner: boolean;
  newGame: () => void;
}

export default class ResultScreen extends React.Component<ResultScreenProps, {}> {
  ...
}
```

