# Dynamic board - async / await
We displayed static images as our battlefield and hand state. In final game this will be provided by our game server. Let's start preparing for that.

## Pet
Will be our key component. That will draw pet card with its attack and defence. We will mark our pets blue and foes red to find them easy.

## GameClient
We will prepare a simple GameClient that will be a facade to access server data in the future. We will return array of pets with their corresponding positions for both hand and battlefield. As this data will be fetched from the server. We mark this functions as async. 

This will mean that they will return a value but after some unknown time. That kind of functions will return a Promise. This is special object that is basically a promise that our function eventually will return something. Promise has, methods we can pass callbacks to. This will allow us to hook to our incoming data.

'then' method is fired when data is returned correctly. It is passed to than callback function and can be used.

```
this.gameClient.getBattlefield().then(battlefield => {
  ...
});
```

Above code mean that getBattlefield function will return promise that it will return some data in the future. We than hook to *then*
method with our callback 

```
battlefield => {
  ...
}
```

So when data arrive it will be passed to *battlefield* variable and we can act on it inside the callback.

'catch' callback function is called when error (failure) is triggered while main function is invoked. This callback allow us to act on error situation.

'finally' callback is always triggered no matter if function run correctly or returned error.

## Setting the state
As you could read previously we can act with our callback when the data is returned. Our Game component is keeping the state of the battlefield so we need to set it upon receiving. For that we use setState method to let React know that something changed.

```
this.gameClient.getBattlefield().then(battlefield => {
  this.setState({
    battlefield
  })
});
```

This way we set battlefield state that we can pass afterwards to other components.
