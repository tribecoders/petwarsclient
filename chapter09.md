# Player rooms
Our players will be fighting one on one. We need to pair them some way. One of the options are Web Socket rooms. First player will create a room with a unique name. The second one will join with the same room name. This way we will bind both of them into a single game. 

We need to design simple player interaction scheme.

## Screens
Our application needs small redesign. We will not display game play screen only. We will keep our **Game** component as our main top component by we will move all game play UI to **PlayScreen**. This way we will display game play area when both players are ready.

The other screens we will introduce are **JoinScreen** where player selects room name to join and **AwaitScreen** where he/she waits for other player to join the game

## Joining a room
When player will select a room he/she will initialise socket connection a await for game status changes

This is our **GameClient** join call

```
join(room: string, onMessage: (message: string) => void) {
  const socket = socketio.connect(":4000/ws");
  socket.emit('create', room);
  socket.on('message', (message: string) => {
    if (message) {
      onMessage(message);
    };
  });
}
```

We initialise socket connection. Emit room joining command and listen for messages.

## State transitions
Our interaction will look like this:
* JoinScreen - on submitting the room we will switch to AwaitingScreen
* AwaitingScreen - when other player will join we will switch to PlayScreen
* PlayScreen - players will play

We can be issued messages from a server. Our game can receive:
* start - starting game play and displaying PlayScreen
* stop - stopping game play (when the other player disconnected) and displaying AwaitingScreen
* full - received when we join full room and are force to select JoinScreen

## Testing
Now it started to get tricky. With 2 player interaction we need to start testing with 2 browser windows. I prefer to use second screen connected to a computer or a mobile device. But it is more up to your convenience. 