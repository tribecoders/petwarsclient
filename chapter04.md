# Rotation

We've made life easier for designers with constant size and ratio of game elements. But with mobile devices we meet with another problem, screen rotation. For our app we need 3 layouts: portrait, landscape and desktop. To simplify this we can made our app always display in portrait mode. 

# Detecting
To detect if screen is rotated we can use simple media query.

```
@media screen and (orientation: portrait)
```

# Displaying portrait as landscape
Now as we can detect orientation we can act on it. If our screen is positioned in landscape view we display as normal fullscreen application. For portrait we can just rotate it with css and swap width and height. 

```
transform: rotate(-90deg);
transform-origin: left top;
height: 100vw;
width: calc(100vh - 100px);
overflow-x: hidden;
position: relative;
left: 0;
top: 100%;
```
# Scaling
Now when we changed the axis for display we need to change the axis when we count the scale.

```
let vpw = window.innerWidth;
    let vph = window.innerHeight;
    // switch axis for portrait view
    if (window.innerWidth < window.innerHeight) {
      vph = window.innerWidth;
      vpw = window.innerHeight - 100;
    }
```


# Browser bars
```
width: calc(100vh - 100px);
```

Why we deduct -100px. This is for browser bars in portrait view. We need to make space for them. We need to take this into account when scaling.