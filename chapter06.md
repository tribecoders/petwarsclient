# Central state management
Our game will become bigger and bigger and passing game game elements state to all require components. We can a central store where we will keep all required data. This can be handled with various state management solutions like Redux, Mobx or Flux. The main idea behind them all is to keep application state in one place and modify it only by common set of actions. This way our state is predictable and easy to debug.

## Mobx
For our project we selected MobX [https://mobx.js.org/](https://mobx.js.org/) as our state manger. It simple to learn its basics and easy to integrate witch class React approach with the decorators (@... functions)

## Store
We will create 2 stores responsible for battlefield and hand state. For now the will be almost empty keeping pet properties for each field. With simple add actions to populate demo data and verify if everything works.

```
export class BattlefieldStore {
  @observable battlefield: IPet[][] = [new Array(6),new Array(6),new Array(6)];

  @action
  setPet(position: IPosition, pet: IPet) {
    this.battlefield[position.row][position.line] = pet
  }
}
```

As you can see we used 2 decorators @observable and @action.

@observable - will let mobx know that we want to keep track of what is stored inside of observed variable. This will come handy when we display observed property.

@action - we mark all methods that will change observable variables state as an action.

## Passing the state
We need to let React know that we will provide some data through the stores. To do that we can use **react-mobx** library with 'Provide' component and surround our game with it.

```
<Provider battlefieldStore={battlefieldStore} handStore={handStore}>
  <Game></Game>
</Provide>
```

## Accessing the stores
Now each component that need to access or change something in one of the stores can inject it to his and observe for the changes. This can be done by further decorators inject and observer. 

```
export interface BattlefieldProps {
  battlefieldStore?: BattlefieldStore
}

@inject('battlefieldStore')
@observer
export default class Battlefield extend React.Component<BattlefieldProps, {}>
```

## Populate default data.
We need to extend our GameClient with a stores access and populate them with demo data.

```
async restore() {
  this.battlefieldStore.setPet({row: 1, line: 1}, {power:1, defence:1, image:'dog', friendly: false});
  this.battlefieldStore.setPet({row: 1, line: 4}, {power:1, defence:1, image:'dog', friendly: true});

  this.handStore.dealPets([
    {power:1, defence:1, image:'dog', friendly: true}, 
    {power:1, defence:1, image:'dog', friendly: true}, 
    {power:1, defence:1, image:'dog', friendly: true}
  ]);
}
```


